
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}

/**
 * Kreiraj nov EHR zapis za pacienta in dodaj osnovne demografske podatke.
 * V primeru uspešne akcije izpiši sporočilo s pridobljenim EHR ID, sicer
 * izpiši napako.
 */
 /*
function kreirajEHRzaBolnika() {
	
	sessionId = getSessionId();

	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
	var datumRojstva = $("#kreirajDatumRojstva").val();

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}

*/
//sdfasdfasdfasdfasdf

/**
 * Pridobivanje vseh zgodovinskih podatkov meritev izbranih vitalnih znakov
 * (telesna temperatura, filtriranje telesne temperature in telesna teža).
 * Filtriranje telesne temperature je izvedena z AQL poizvedbo, ki se uporablja
 * za napredno iskanje po zdravstvenih podatkih.
 */
 /*
 var btnPritisk = document.getElementById("preberiPritisk");
 btnPritisk.addEventListener('click', function(){
    preberiMeritveVitalnihZnakov("blood pressure");
});
//document.body.appendChild(btnPritisk);


var btnTemperatura = document.getElementById("preberiTemp");
 btnTemperatura.addEventListener('click', function(){
    preberiMeritveVitalnihZnakov("telesna temperatura");
});
//document.body.appendChild(btnTemperatura);


var btnTeza = document.getElementById("preberitezo");
 btnTeza.addEventListener('click', function(){
    preberiMeritveVitalnihZnakov("telesna teža");
});
//document.body.appendChild(preberitezo);
*/
/*
function preberiMeritveVitalnihZnakov(tip) {
	var podatki;
	var arr = [];
	sessionId = getSessionId();

	var ehrId = $("#preberiEhrID").val().trim();
	//var tip = $("#preberiTipZaVitalneZnake").val();
	//tip = "blood pressure";

	if (!ehrId || ehrId.trim().length == 0 || !tip || tip.trim().length == 0) {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#helth_warning").html("<br/><span>Pridobivanje " +
          "podatkov za <b>'" + tip + "'</b> bolnika <b>'" + party.firstNames +
          " " + party.lastNames + "'</b>.</span><br/><br/>");
          
				if (tip == "blood pressure") {
			
					
					console.log("pred vpisom!!!!");
					$.ajax({
					
						
  						url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure",   /*body_temperature*/
  						/*
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	console.log("zdaj bodo resultazi");
					    		//console.log(res);
					    	if (res.length > 0) {
						    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>diastolični krvni pritisk</th></tr>";
						        for (var i in res) {
						            results += "<tr><td>" + res[i].time +
                          "</td><td class='text-right'>" + res[i].diastolic +
                          " " + res[i].unit + "</td>";
						        }
						        results += "</table>";
						        $("#content").empty();
						        $("#content").append(results);
								var arr = [];
								var len = res.length;
								for (var i = 0; i < len; i++) {
									arr.push({
        								y: res[i].diastolic,
        								label: res[i].time,
        								enote: res[i].unit,
        								tip: "diastolic blood presure"
									});
								}
								require("knjiznice/js/graf.js");
								console.log(arr[0].tip + " to je tip");
								izrisGrafa(arr);
					    	} else {
					    		$("#content").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
				} else if (tip == "telesna temperatura") {
					$.ajax({
  					    url: baseUrl + "/view/" + ehrId + "/" + "body_temperature",   /*body_temperature*/
  					    /*
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
						    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna temperatura</th></tr>";
						        for (var i in res) {
						            results += "<tr><td>" + res[i].time +
                          "</td><td class='text-right'>" + res[i].temperature +
                          " " + res[i].unit + "</td>";
						        }
						        results += "</table>";
						        $("#content").empty();
						        $("#content").append(results);
						        						        var arr = [];
								var len = res.length;
								for (var i = 0; i < len; i++) {
									arr.push({
        								y: res[i].temperature,
        								label: res[i].time,
        								enote: res[i].unit,
        								tip: "telesna temperatura"
									});
								}
								require("knjiznice/js/graf.js");
								console.log(arr[0].y + " to je y");
								izrisGrafa(arr);
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
				}  else if (tip == "telesna teža") {
					$.ajax({
					    url: baseUrl + "/view/" + ehrId + "/" + "weight",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
						    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna teža</th></tr>";
						        for (var i in res) {
						            results += "<tr><td>" + res[i].time +
                          "</td><td class='text-right'>" + res[i].weight + " " 	+
                          res[i].unit + "</td>";
						        }
						        results += "</table>";
						       $("#content").empty();
						        $("#content").append(results);
						    	var arr = [];
								var len = res.length;
								for (var i = 0; i < len; i++) {
									arr.push({
        								y: res[i].weight,
        								label: res[i].time,
        								enote: res[i].unit,
        								tip: "telesna teža"
									});
								}
								require("knjiznice/js/graf.js");
								console.log(arr[0].y + " to je y");
								izrisGrafa(arr);
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
				} // tukaj je bil še aql poizvedba za neki els if...
	    	},
	    	error: function(err) {
	    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
	}
	
	/*
	var arr = [];
	var len = oFullResponse.results.length;
	for (var i = 0; i < len; i++) {
		arr.push({
        	y: stevilka,
        	label: datum,
        	enote: enote
		});
	}
	*/
	
/*	 
}
*/
//sfdhsghsfdhsgfhsfghgf

var bloodPressureD = [];
var bloodPressureS = [];
var bodyTemp = [];
var bodyWeight = [];
function preberiVseMeritve(izbira){
	
	var podatki;
	var arr = [];
	sessionId = getSessionId();
if(izbira == "a"){	var ehrId = $("#preberiEhrID").val().trim();
	//var element = document.getElementById('leaveCode');
    //element.value = ;
}
else{
	var e = document.getElementById("dDown");
	var ehrId  = e.options[e.selectedIndex].value.trim();
	console.log("v elsu" + ehrId);
	
}
	//var tip = $("#preberiTipZaVitalneZnake").val();
	//tip = "blood pressure";

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				/*
				$("#helth_warning").html("<br/><span>Pridobivanje " +
          "podatkov za <b>'" + "tukaj naj bi bil tip!!" + "'</b> bolnika <b>'" + party.firstNames +
          " " + party.lastNames + "'</b>.</span><br/><br/>");
          
				*/
			
					
					console.log("pred vpisom!!!!");
					$.ajax({
					
						
  						url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure",   /*body_temperature*/
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res1) {
					    	console.log("zdaj bodo resultazi");
					    		//console.log(res);
					    	if (res1.length > 0) {
					    		bloodPressureD = res1;
					    		console.log(bloodPressureD);
						    	preberiIzbranoMeritev("blood pressure");
						    	
					    	} else {
					    		$("#content").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
					
					$.ajax({
					
						
  						url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure",   /*body_temperature*/
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res1) {
					    	console.log("zdaj bodo resultazi");
					    		//console.log(res);
					    	if (res1.length > 0) {
					    		bloodPressureS = res1;
					    		console.log(bloodPressureS);
						    	
					    	} else {
					    		$("#content").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
					
				 
					$.ajax({
  					    url: baseUrl + "/view/" + ehrId + "/" + "body_temperature",   /*body_temperature*/
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res2) {
					    	if (res2.length > 0) {
					    		bodyTemp = res2;
					    		
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
				  
					$.ajax({
					    url: baseUrl + "/view/" + ehrId + "/" + "weight",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res3) {
					    	if (res3.length > 0) {
					    		bodyWeight = res3;
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
				 // tukaj je bil še aql poizvedba za neki els if...
	    	},
	    	error: function(err) {
	    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
	}
	
	/*
	var arr = [];
	var len = oFullResponse.results.length;
	for (var i = 0; i < len; i++) {
		arr.push({
        	y: stevilka,
        	label: datum,
        	enote: enote
		});
	}
	*/
	
	 

	
	
	
	
	
	
	
	
}

function preveriMeritve(){
	
	
	if(bloodPressureS[0].systolic < 120){
		
		$("#helthLeft").html("<br/><span>vaš sistoličen pritisk je vredu  </span><br/><br/>" );
		
	 }
	 else if(bloodPressureS[0].systolic >= 120 && bloodPressureS[0].systolic <=139){
	 	$("#helthLeft").html("<br/><span>vaš sistoličen pritisk je rahlo povečan  </span><br/><br/>" );
	 }
	 else if(bloodPressureS[0].systolic >= 140 && bloodPressureS[0].systolic <=159){
	 	$("#helthLeft").html("<br/><span>vaš sistoličen pritisk je visok, predlagamo posvetovanje z osebnim zdravnikom  </span><br/><br/>" );
	 }
	 else if(bloodPressureS[0].systolic >= 160 && bloodPressureS[0].systolic <=180){
	 	$("#helthLeft").html("<br/><span>vaš sistoličen pritisk je postaja nevaren, obvezno se posvetute z zdravnikom  </span><br/><br/>" );
	 }
	 else if(bloodPressureS[0].systolic >= 180){
	 	$("#helthLeft").html("<br/><span>vaš sistoličen pritisk je zelo prevelik, takoj poiščite medicinsko pomoč!! </span><br/><br/>" );
	 }
	 
	 
	 
	 if(bloodPressureD[0].diastolic < 80) {
	 	$("#helthRight").html("<br/><span>vaš diastoličen pritisk je vredu  </span><br/><br/>" );
	 	
		} 
		else if(bloodPressureD[0].diastolic >= 80 && bloodPressureD[0].diastolic <= 89) {
	 	$("#helthRight").html("<br/><span>vaš diastoličen pritisk je rahlo povečan  </span><br/><br/>" );
		}
		else if(bloodPressureD[0].diastolic >= 90 && bloodPressureD[0].diastolic <= 99) {
	 	$("#helthRight").html("<br/><span>vaš diastoličen pritisk je visok, predlagamo posvetovanje z osebnim zdravnikom  </span><br/><br/>" );
		}
		else if(bloodPressureD[0].diastolic >= 100 && bloodPressureD[0].diastolic < 110) {
			$("#helthRight").html("<br/><span>vaš diastoličen pritisk je postaja nevaren, obvezno se posvetute z zdravnikom  </span><br/><br/>" );
		}
		else if(bloodPressureD[0].diastolic >= 110) {
		$("#helthRight").html("<br/><span>vaš diastoličen pritisk je zelo prevelik, takoj poiščite medicinsko pomoč!! </span><br/><br/>" );
		}
	
}

function preberiIzbranoMeritev(tip){

	preveriMeritve();

	
		
				if (tip == "blood pressure") {
				    	if (bloodPressureD.length > 0) {
					    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>diastolični krvni pritisk</th></tr>";
						        for (var i in bloodPressureD) {
						            results += "<tr><td>" + bloodPressureD[i].time +
                          "</td><td class='text-right'>" + bloodPressureD[i].diastolic +
                          " " + bloodPressureD[i].unit + "</td>";
						        }
						        results += "</table>";
						        $("#content").empty();
						        $("#content").append(results);
								var arr = [];
								var len = bloodPressureD.length;
								for (var i = 0; i < len; i++) {
									arr.push({
        								y: bloodPressureD[i].diastolic,
        								label: bloodPressureD[i].time,
        								enote: bloodPressureD[i].unit,
        								tip: "diastolični krvni pritisk"
									});
								}
								require("knjiznice/js/graf.js");
								console.log(arr[0].tip + " to je tip");
								izrisGrafa(arr);
					    	} else {
					    		$("#content").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    
					    
					
				} else if (tip == "blood pressureS") {
				    	if (bloodPressureS.length > 0) {
					    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>sistolični krvni pritisk</th></tr>";
						        for (var i in bloodPressureS) {
						            results += "<tr><td>" + bloodPressureS[i].time +
                          "</td><td class='text-right'>" + bloodPressureS[i].systolic +
                          " " + bloodPressureS[i].unit + "</td>";
						        }
						        results += "</table>";
						        $("#content").empty();
						        $("#content").append(results);
								var arr = [];
								var len = bloodPressureS.length;
								for (var i = 0; i < len; i++) {
									arr.push({
        								y: bloodPressureS[i].systolic,
        								label: bloodPressureS[i].time,
        								enote: bloodPressureS[i].unit,
        								tip: "sistolični krvni pritisk"
									});
								}
								require("knjiznice/js/graf.js");
								console.log(arr[0].tip + " to je tip");
								izrisGrafa(arr);
					    	} else {
					    		$("#content").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    
					    
					
				}
				else if (tip == "telesna temperatura") {
				
					    	if (bodyTemp.length > 0) {
						    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna temperatura</th></tr>";
						        for (var i in bodyTemp) {
						            results += "<tr><td>" + bodyTemp[i].time +
                          "</td><td class='text-right'>" + bodyTemp[i].temperature +
                          " " + bodyTemp[i].unit + "</td>";
						        }
						        results += "</table>";
						        $("#content").empty();
						        $("#content").append(results);
						        						        var arr = [];
								var len = bodyTemp.length;
								for (var i = 0; i < len; i++) {
									arr.push({
        								y: bodyTemp[i].temperature,
        								label: bodyTemp[i].time,
        								enote: bodyTemp[i].unit,
        								tip: "telesna temperatura"
									});
								}
								require("knjiznice/js/graf.js");
								console.log(arr[0].y + " to je y");
								izrisGrafa(arr);
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					  
				}  else if (tip == "telesna teža") {
					
					    	if (bodyWeight.length > 0) {
						    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna teža</th></tr>";
						        for (var i in bodyWeight) {
						            results += "<tr><td>" + bodyWeight[i].time +
                          "</td><td class='text-right'>" + bodyWeight[i].weight + " " 	+
                          bodyWeight[i].unit + "</td>";
						        }
						        results += "</table>";
						       $("#content").empty();
						        $("#content").append(results);
						    	var arr = [];
								var len = bodyWeight.length;
								for (var i = 0; i < len; i++) {
									arr.push({
        								y: bodyWeight[i].weight,
        								label: bodyWeight[i].time,
        								enote: bodyWeight[i].unit,
        								tip: "telesna teža"
									});
								}
								require("knjiznice/js/graf.js");
								console.log(arr[0].y + " to je y");
								izrisGrafa(arr);
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    
					    
					
				} // tukaj je bil še aql poizvedba za neki els if...
	    
	    	
		
	}
	
	/*
	var arr = [];
	var len = oFullResponse.results.length;
	for (var i = 0; i < len; i++) {
		arr.push({
        	y: stevilka,
        	label: datum,
        	enote: enote
		});
	}
	*/
	
	 

	
	





/**
 * izrisovanje grafa!!
 *
 */
 function require(script) {
    $.ajax({
        url: script,
        dataType: "script",
        async: false,           // <-- This is the key
        success: function () {
            // all good...
        },
        error: function () {
            throw new Error("Could not load script " + script);
        }
    });
}





/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
 var tabelaPodatkov = [
 	{ime:"janez",priimek:"lukic", datumRojstva:"1998-10-30T14:55",datumInU: "", telesnaVisina : 1,telesnaTeza: 1, telesnaTemp:1 , sistolicniKrvniTlak:1 , diastolicniKrvniTlak: 1, nasicenostKrviSKisikom:1},
 	{ime:"timotej",priimek:"kocevar", datumRojstva:"1980-11-11T01:30",datumInU: "", telesnaVisina :1 ,telesnaTeza: 1, telesnaTemp: 11, sistolicniKrvniTlak:1 , diastolicniKrvniTlak: 1, nasicenostKrviSKisikom:1},
 	{ime:"micka",priimek:"knez", datumRojstva:"1992-01-15T15:15",datumInU: "", telesnaVisina : 1,telesnaTeza: 1, telesnaTemp:1 , sistolicniKrvniTlak: 1, diastolicniKrvniTlak: 1, nasicenostKrviSKisikom:1}
 	];
 var tabelaEHR = [];
 window.onload = function(){kreirajEHR();
 	preberiVseMeritve('b');
 	preberiVseMeritve('b');
 };
 function kreirajEHR(){
 	kreirajEHRzaBolnika(tabelaPodatkov[0].ime ,tabelaPodatkov[0].priimek,tabelaPodatkov[0].datumRojstva);
 	kreirajEHRzaBolnika(tabelaPodatkov[1].ime,tabelaPodatkov[1].priimek,tabelaPodatkov[1].datumRojstva);
 	 kreirajEHRzaBolnika(tabelaPodatkov[2].ime,tabelaPodatkov[2].priimek,tabelaPodatkov[2].datumRojstva);
 	
 }
 
 function kreiraj(){
 	//tabelaPodatkov.length
 	//tabelaPodatkov.length
 	//tabelaPodatkov.length
 	if(tabelaEHR[0] != null && tabelaEHR[1] != null && tabelaEHR[2] != null) {
 	console.log("tabela je " + tabelaEHR[0]);
 	require("knjiznice/js/jquery-2.2.3.min.js");
	$("#dDown").empty();
	
 	var ehrID2;
 	for(var i = 0; i < tabelaPodatkov.length ; i++){
 		
 		
 		for(var k = 0; k < 9; k++){
 			
 				
 				var y = Math.floor(Math.random() * (2016 - 1935)) + 1935;
 				var m = Math.floor(Math.random() * (12 - 1)) + 1;
 				var d = Math.floor(Math.random() * (29 - 1)) + 1;
 				var h = Math.floor(Math.random() * (23 - 0)) + 0;
 				var min = Math.floor(Math.random() * (59 - 0)) + 0;
 				tabelaPodatkov[i].datumInU = y +"-" +m+"-" +d+"T"+h+":"+min
 				tabelaPodatkov[i].telesnaVisina = Math.floor(Math.random() * (200 - 155)) + 155;
 				tabelaPodatkov[i].telesnaTeza = Math.floor(Math.random() * (190 - 40)) + 40;
 				tabelaPodatkov[i].telesnaTemp = 35;
 				if(i == 0){
 						tabelaPodatkov[i].sistolicniKrvniTlak = Math.floor(Math.random() * (190 - 140)) + 140;
 						tabelaPodatkov[i].diastolicniKrvniTlak = Math.floor(Math.random() * (95 - 85)) + 85;
 				} else if ( i == 1 ) {
 					tabelaPodatkov[i].sistolicniKrvniTlak = Math.floor(Math.random() * (145 - 122)) + 122;
 						tabelaPodatkov[i].diastolicniKrvniTlak = Math.floor(Math.random() * (115 - 95)) + 95;
 				} else {
 					tabelaPodatkov[i].sistolicniKrvniTlak = Math.floor(Math.random() * (119 - 90)) + 90;
 						tabelaPodatkov[i].diastolicniKrvniTlak = Math.floor(Math.random() * (79 - 60)) + 60;
 				}
 			 // 119 or less
 				 // 79 or less
 				tabelaPodatkov[i].nasicenostKrviSKisikom = 95; //95 -100;
 				//if(k == 0)	generirajPodatke(, tabelaPodatkov[i]);
 				//else {
 					console.log("herrrr ="+tabelaEHR[i]);
 					setTimeout(generirajPodatke(tabelaEHR[i], tabelaPodatkov[i]), 250);
 				//	generirajPodatke(tabelaEHR[i], tabelaPodatkov[i]);
 				//}
 		}
 		console.log("egr je " + tabelaEHR[i]);
 		var select = document.getElementById("dDown");
			select.options[select.options.length] = new Option(tabelaPodatkov[i].ime + " " +tabelaPodatkov[i].priimek, ""+tabelaEHR[i]);
 		
 		//tabelaEHR[i] = ehrID2;
 		//if(true){
 			
 			//while(ehrID3 == "undefined") {
 				
 			//}
 			//if(ehrID3 != "undefined") {
 			//}
 		//}
 			
 		
 		
 		
 		/*waitt(tabelaPodatkov[i], function(ime,priim,ehrID33){
 			var select = document.getElementById("dDown");
			select.options[select.options.length] = new Option(ime + " " + priim, ""+ehrID33);
 			
 		});
 		*/
 	


 	}
 	
 		
 	} else {
 		console.log("ERRRRRRRRR");
 	}
 	
 }
 /*
 function waitt(podatkii,callback){
 	var ehrID3 = kreirajEHRzaBolnika(podatkii.ime,podatkii.priimek,podatkii.datumRojstva);
 	
 	callback(podatkii.ime, podatkii.priimek, ehrID3);
 }
 
 function after(ime, priim,ehrID33){
 	var select = document.getElementById("dDown");
	select.options[select.options.length] = new Option(ime + " " + priim, ""+ehrID33);
 }
 */
 
 function kreirajEHRzaBolnika(imee,priim,datumR) {
	sessionId = getSessionId();

	var ime = imee;
	var priimek = priim;
	var datumRojstva = datumR;

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                  //  $("#kreirajSporocilo").html("<span class='obvestilo " +
                          //"label label-success fade-in'>Uspešno kreiran EHR '" +
                          //ehrId + "'.</span>");
		                   // $("#preberiEHRid").val(ehrId);
		                   console.log("pravi ehr je " + ehrId);
		                   tabelaEHR.push(ehrId);
		                   return ehrId;
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}
 
 
 
 
 
 
function generirajPodatke(stPacienta,podatki) {
 // ehrId = "";
sessionId = getSessionId();

	var ehrId = stPacienta;
	var datumInUra = podatki.datumInU;
	var telesnaVisina = podatki.telesnaVisina;
	var telesnaTeza = podatki.telesnaTeza;
	var telesnaTemperatura = podatki.telesnaTemp;
	var sistolicniKrvniTlak = podatki.sistolicniKrvniTlak;
	var diastolicniKrvniTlak = podatki.diastolicniKrvniTlak;
	var nasicenostKrviSKisikom = podatki.nasicenostKrviSKisikom;
	var merilec = "";

	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: merilec
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#dodajMeritveVitalnihZnakovSporocilo").html(
              "<span class='obvestilo label label-success fade-in'>" +
              res.meta.href + ".</span>");
		    },
		    error: function(err) {
		    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
	}
  // TODO: Potrebno implementirati

  return ehrId;
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
