function require(script) {
    $.ajax({
        url: script,
        dataType: "script",
        async: false,           // <-- This is the key
        success: function () {
            // all good...
        },
        error: function () {
            throw new Error("Could not load script " + script);
        }
    });
}


function izrisGrafa(podatki){
  require("knjiznice/js/canvasjs.min.js");
  //alert("dela?");
  console.log(podatki[0]);
 var items = [
        { y: 198, label: "Itallll"},
        { y: 201, label: "China"},
        { y: 202, label: "France"},        
        { y: 236, label: "Great Britain"},        
        { y: 395, label: "Soviet Union"},        
        { y: 957, label: "USA"}
];
    var chart = new CanvasJS.Chart("graphs",
    {
      title:{
        text: podatki[0].tip
      },
      animationEnabled: true,
      legend: {
        cursor:"pointer",
        itemclick : function(e) {
          if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
              e.dataSeries.visible = false;
          }
          else {
              e.dataSeries.visible = true;
          }
          chart.render();
        }
      },
      axisY: {
        title: ""
      },
      toolTip: {
        shared: true,  
        content: function(e){
          var str = '';
          var total = 0 ;
          var str3 = "";
          var str2 ;
          for (var i = 0; i < e.entries.length; i++){
            var  str1 = "<span style= 'color:"+e.entries[i].dataSeries.color + "'> " + e.entries[i].dataPoint.tip + "</span>: <strong>"+  e.entries[i].dataPoint.y +" "+ e.entries[i].dataPoint.enote + "</strong> <br/>" ; 
            total = e.entries[i].dataPoint.y + total;
            str = str.concat(str1);
          }
          str2 = "<span style = 'color:DodgerBlue; '><strong>"+e.entries[0].dataPoint.label + "</strong></span><br/>";
          //str3 = "<span style = 'color:Tomato '>Total: </span><strong>" + total + "</strong><br/>";
          // str4 dodajam kar hocem
          return (str2.concat(str)).concat(str3);
        }

      },
      data: [
      {        
        type: "bar",
        showInLegend: true,
        name: "vrednosti",
        color: "#0fbf26",
        dataPoints: podatki
      }

      ]
    });

chart.render();
}